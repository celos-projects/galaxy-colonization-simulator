#ifndef COORD_HPP_
#define COORD_HPP_

class Coord {
    public:
        Coord(float x, float y, float z);
        
        void getCoord(float* c);

        static float dist(Coord p1, Coord p2);
        static Coord randCoord(float maxX, float maxY, float maxZ);

        float getX() const;
        float getY() const;
        float getZ() const;

    private:
        const float x;
        const float y;
        const float z;
};

#endif
