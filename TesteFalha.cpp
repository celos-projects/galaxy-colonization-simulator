#include <cstdlib>

#include "TesteFalha.hpp"

TesteFalha::TesteFalha(Missao* m, double chance)
    :Evento(m->getTempoSaida()), missao{m}, chance{chance}
{
}

bool TesteFalha::executar()
{
    if((std::rand() / RAND_MAX) <= this->chance){
        this->missao->setAndamento(false);
        this->missao->setFalha(true);
        return false;
    }else{
        return true;
    }
}
