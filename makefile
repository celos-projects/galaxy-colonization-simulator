parametrosCompilacao=-Wall #-Wshadow
nomePrograma=main

all: $(nomePrograma)

#objects=main.o Pessoa.o Administrador.o Cliente.o Operacao.o Console.o CPFInvalidoException.o IdadeInvalidaException.o NomeInvalidoException.o SenhaInvalidaException.o
objects=main.o Coord.o Sistema.o Galaxia.o Missao.o

$(nomePrograma): $(objects)
	g++ -o $(nomePrograma) $(objects) $(parametrosCompilacao)

main.o: main.cpp
	g++ -c main.cpp $(parametrosCompilacao)

Coord.o: Coord.hpp Coord.cpp
	g++ -c Coord.cpp $(parametrosCompilacao)

Sistema.o: Sistema.hpp Sistema.cpp
	g++ -c Sistema.cpp $(parametrosCompilacao)

Galaxia.o: Galaxia.hpp Galaxia.cpp
	g++ -c Galaxia.cpp $(parametrosCompilacao)

Missao.o: Missao.hpp Missao.cpp
	g++ -c Missao.cpp $(parametrosCompilacao)

clean:
	rm -f *.o *.gch $(nomePrograma)
