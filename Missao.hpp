#ifndef MISSAO_HPP_
#define MISSAO_HPP_

#include "Sistema.hpp"

class Missao {
    public:
        Missao(Sistema* origem, Sistema* destino, double tempoSaida, double tempoChegada);

        Sistema& getOrigem() const;
        Sistema& getDestino() const;

        double getTempoSaida() const;
        double getTempoChegada() const;

        bool getSucesso() const;
        bool getFalha() const;
        bool getEmAndamento() const;

    private:
		static unsigned long proxId;

		const unsigned long id;

        Sistema* const origem;
        Sistema* const destino;
        
        const double tempoSaida;
        const double tempoChegada;

        bool sucesso;
        bool falha;
        bool andamento;
};

#endif
