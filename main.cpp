#include <iostream>
#include <list>
#include <cstdlib>

#include "Galaxia.hpp"
#include "Sistema.hpp"
#include "Missao.hpp"

#define TAM_GALAXIA         2
#define DENSIDADE_ESTRELAS  0.2
#define VELOCIDADE_NAVE 
#define TEMPO_TESTE_FALHA   350
#define CHANCE_FALHA        0.5 

void falhaMemoriaDinamica();

int main() {
    std::srand(1);

    double anoAtual{0};

    Galaxia* g{nullptr};
    try{
        g = new Galaxia{"Mahalla Jones", DENSIDADE_ESTRELAS, TAM_GALAXIA, TAM_GALAXIA, TAM_GALAXIA};
    }catch(std::bad_alloc& e){
        falhaMemoriaDinamica();
        delete g;
    }

    for(size_t i{0}; i < g->getNumSistemas(); i++){
        try{
            g->inserirSistema(new Sistema{TAM_GALAXIA, TAM_GALAXIA, TAM_GALAXIA});
        }catch(std::bad_alloc& e){
            falhaMemoriaDinamica();
            delete g;
        }
    }

    //Sistema* sisVizinho;
    //float dist;
    //std::list<Sistema*>::iterator it{g->getSistemas().begin()};
    //for(; it !=  g->getSistemas().end(); it++){
    //    sisVizinho = g->getSistemaVizinho(*it, &dist);
    //    std::cout << "--SISTEMAS VIZINHOS--" << std::endl;
    //    std::cout << "Sistema " << (*it)->getId();
    //    if(! sisVizinho)
    //        std::cout << " nao possui vizinho" << std::endl;
    //    else
    //        std::cout << " e vizinho do Sistema " << sisVizinho->getId() << " e a distancia e de " << dist << std::endl;
    //    std::cout << std::endl;

    //    sisVizinho = g->getSistemaVizinhoVazio(*it, &dist);
    //    std::cout << "Sistema " << (*it)->getId();
    //    if(! sisVizinho)
    //        std::cout << " nao possui vizinho vazio" << std::endl;
    //    else
    //        std::cout << " e vizinho do Sistema " << sisVizinho->getId() << " desocupado e a distancia e de " << dist << std::endl;
    //    std::cout << std::endl;

    //    std::cout << std::endl;
    //}

    delete g;

    return 0;
}

void falhaMemoriaDinamica()
{
    std::cout << "Falha de alocacao de memoria dinamica. Encerrando execucao do programa" << std::endl;
}
