#ifndef EVENTO_HPP_
#define EVENTO_HPP_

class Evento {
    public:
        Evento(double momento);

        virtual void executar();

    private:
        double momento;
}

#endif
