#ifndef PARTIDA_HPP_
#define PARTIDA_HPP_

#include "Evento.hpp"
#include "Missao.hpp"

class Partida: public Evento {
    public:
        Partida(Missao* m);

    private:
        Missao* missao;
}

#endif
