#include <cmath>
#include <cstdlib>

#include "Coord.hpp"

Coord::Coord(float x, float y, float z)
    :x{x}, y{y}, z{z} {
}

float Coord::dist(Coord p1, Coord p2)
{
    return sqrt(pow(p2.getX() - p1.getX(), 2) + pow(p2.getY() - p1.getY(), 2) + pow(p2.getZ() - p1.getZ(), 2));
}

Coord Coord::randCoord(float maxX, float maxY, float maxZ)
{
    float x = static_cast<float>(std::rand()) / static_cast<float>(RAND_MAX) * maxX;
    float y = static_cast<float>(std::rand()) / static_cast<float>(RAND_MAX) * maxY;
    float z = static_cast<float>(std::rand()) / static_cast<float>(RAND_MAX) * maxZ;
    Coord c{x, y, z};

    return c;
}

void Coord::getCoord(float* c) 
{
    c[0] = this->getX();
    c[1] = this->getY();
    c[2] = this->getZ();
}

float Coord::getX() const
{
    return this->x;
}

float Coord::getY() const
{
    return this->y;
}

float Coord::getZ() const
{
    return this->z;
}
