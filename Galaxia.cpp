#include <iostream>

#include "Galaxia.hpp"

Galaxia::Galaxia(const std::string& nome, const float densidadeDeSistemas, const float tamX, const float tamY, const float tamZ)
    :sistemas{new std::list<Sistema*>},
    nome{nome}, densidadeDeSistemas{densidadeDeSistemas}, tamX{tamX}, tamY{tamY}, tamZ{tamZ},
    numSistemas{static_cast<size_t>(tamX * tamY * tamZ * densidadeDeSistemas)}
{
}

Galaxia::~Galaxia()
{
    std::list<Sistema*>::iterator it{this->sistemas->begin()};

    for(; it != this->sistemas->end(); it++)
        delete *it;

    delete this->sistemas;
}

bool Galaxia::inserirSistema(Sistema* sis)
{
    if(this->sistemas->size() >= this->numSistemas)
        return false;

    std::list<Sistema*>::iterator it{this->sistemas->begin()};

    for(; (it != this->sistemas->end()) && ((*it)->getId() < sis->getId()); it++);

    if(it == this->sistemas->end()){
        this->sistemas->insert(it, sis);
        return true;
    }

    std::cout << "Falha ao Inserir. Sistema " << sis->getId() << " ja existe.\n";
    return false;
}

Sistema* Galaxia::getSistemaVizinho(const Sistema* sis, float* dist)
{
    Sistema* sisMaisProximo = nullptr;
    float menorDist = this->tamX + this->tamY + this->tamZ;
    float novaDist;

    std::list<Sistema*>::iterator it{this->sistemas->begin()};
    for(; it != this->sistemas->end(); it++){
        if(*it != sis){
            novaDist = Sistema::dist(sis, *it);
            if(novaDist < menorDist){
                menorDist = novaDist;
                sisMaisProximo = *it;
            }
        }
    }

    *dist = menorDist;
    return sisMaisProximo;
}

Sistema* Galaxia::getSistemaVizinhoVazio(const Sistema* sis, float* dist)
{
    Sistema* sisMaisProximo = nullptr;
    float menorDist = this->tamX + this->tamY + this->tamZ;
    float novaDist;

    std::list<Sistema*>::iterator it{this->sistemas->begin()};
    for(; it != this->sistemas->end(); it++){
        if((*it != sis) && (! (*it)->getOcupado())){
            novaDist = Sistema::dist(sis, *it);
            if(novaDist < menorDist){
                menorDist = novaDist;
                sisMaisProximo = *it;
            }
        }
    }

    *dist = menorDist;
    return sisMaisProximo;
}

std::list<Sistema*>& Galaxia::getSistemas() const
{
    return *(this->sistemas);
}

size_t Galaxia::getNumSistemas() const
{
    return this->numSistemas;
}

const std::string& Galaxia::getNome() const
{
    return this->nome;
}

void Galaxia::imprimirGalaxia() const
{
    std::cout << "Galaxia " << this->getNome() << std::endl;   
    std::list<Sistema*>::iterator it{this->sistemas->begin()};
    for(; it != this->sistemas->end(); it++)
        (*it)->imprimirSistema(); 
}
