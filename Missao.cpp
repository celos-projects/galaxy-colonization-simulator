#include "Missao.hpp"

unsigned long Missao::proxId{1};

Missao::Missao(Sistema* origem, Sistema* destino, double tempoSaida, double tempoChegada)
    :id{Missao::proxId},
    origem{origem}, destino{destino}, tempoSaida{tempoSaida}, tempoChegada{tempoChegada},
    sucesso{false}, falha{false}, andamento{true}
{
    Missao::proxId++;
} 

Sistema& Missao::getOrigem() const
{
    return *(this->origem);
}

Sistema& Missao::getDestino() const
{
    return *(this->destino);
}

double Missao::getTempoSaida() const
{
    return this->tempoSaida;
}

double Missao::getTempoChegada() const
{
    return this->tempoChegada;
}

bool Missao::getSucesso() const
{
    return this->sucesso;
}

bool Missao::getFalha() const
{
    return this->falha;
}

bool Missao::getEmAndamento() const
{
    return this->andamento;
}
