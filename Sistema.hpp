#ifndef SISTEMA_HPP_
#define SISTEMA_HPP_

#include <list>

#include "Coord.hpp"

class Missao;
class Sistema {
	public:
		explicit Sistema(Coord pos);
        Sistema(float maxX, float maxY, float maxZ);

        unsigned long getId() const;

        void inserirMissaoRecebida(Missao* m);
        std::list<Missao*>& getMissoesRecebidas() const;

        void inserirMissaoEnviada(Missao* m);
        std::list<Missao*>& getMissoesEnviadas() const;

        bool getEmAndamento() const;
        double getTempoEmAndamento() const;

        bool getOcupado() const;
        double getTempoOcupado() const;

        bool getColonizado() const;
        void setColonizado(bool estado);
        double getTempoColonizado() const;
        void setTempoColonizado(double tempo);

        static float dist(const Sistema* s1, const Sistema* s2);

        Coord getCoord() const;
        void imprimirSistema() const;

	private:
		static unsigned long proxId;

		const unsigned long id;

        bool colonizado;
        double tempoColonizado;

        std::list<Missao*>* missoesRecebidas;
        std::list<Missao*>* missoesEnviadas;

        const Coord pos;
};

#endif
