#ifndef TESTE_FALHA_HPP_
#define TESTE_FALHA_HPP_

#include "Evento.hpp"
#include "Missao.hpp"

class TesteFalha: public Evento {
    public:
        explicit TesteFalha(Missao* m, double chance);

    private:
        Missao* missao;

        const double chance;
};

#endif
