#ifndef GALAXIA_HPP_
#define GALAXIA_HPP_

#include <list>
#include <string>

#include "Sistema.hpp"

class Galaxia {
    public:
        Galaxia(const std::string& nome, const float densidadeDeEstrelas, const float tamX, const float tamY, const float tamZ);

        ~Galaxia();

        bool inserirSistema(Sistema* sis);

        Sistema* getSistemaVizinho(const Sistema* sis, float* dist);
        Sistema* getSistemaVizinhoVazio(const Sistema* sis, float* dist);

        std::list<Sistema*>& getSistemas() const;

        size_t getNumSistemas() const;

        const std::string& getNome() const;

        void imprimirGalaxia() const;

        //bool salvarGalaxia(std::string& nomeArq const) const;

    private:
        std::list<Sistema*>* sistemas;
        const std::string nome;
        const float densidadeDeSistemas;
        const float tamX;
        const float tamY;
        const float tamZ;
        const size_t numSistemas;
};

#endif
