#include <iostream>
#include <iomanip>

#include "Sistema.hpp"
#include "Missao.hpp"

unsigned long Sistema::proxId{1};

Sistema::Sistema(Coord pos)
	:id{Sistema::proxId},
    missoesRecebidas{new std::list<Missao*>}, missoesEnviadas{new std::list<Missao*>},
    pos{pos}
{
	Sistema::proxId++;
}

Sistema::Sistema(float maxX, float maxY, float maxZ)
	:id{Sistema::proxId}, pos{Coord::randCoord(maxX, maxY, maxZ)}
{
	Sistema::proxId++;
}

unsigned long Sistema::getId() const
{
    return this->id;
}

void Sistema::inserirMissaoRecebida(Missao* m)
{
    this->getMissoesRecebidas().push_back(m);
}

std::list<Missao*>& Sistema::getMissoesRecebidas() const
{
    return *(this->missoesRecebidas);
}

void Sistema::inserirMissaoEnviada(Missao* m)
{
    this->getMissoesEnviadas().push_back(m);
}

std::list<Missao*>& Sistema::getMissoesEnviadas() const
{
    return *(this->missoesEnviadas);
}

bool Sistema::getEmAndamento() const
{
    if(! this->getMissoesRecebidas().empty()){
        Missao* m{this->getMissoesRecebidas().back()};
        if((! m) || (! m->getEmAndamento()))
            return false;
        
        return true;
    }

    return false;
}

double Sistema::getTempoEmAndamento() const
{
    return this->getMissoesRecebidas().back()->getTempoSaida();
}

bool Sistema::getOcupado() const
{
    if(! this->getMissoesRecebidas().empty()){
        Missao* m{this->getMissoesRecebidas().back()};
        if((! m) || (! m->getSucesso()))
            return false;
        
        return true;
    }

    return false;
}

double Sistema::getTempoOcupado() const
{
    return this->getMissoesRecebidas().back()->getTempoChegada();
}

bool Sistema::getColonizado() const
{
    return this->colonizado;
}

void Sistema::setColonizado(bool estado)
{
    this->colonizado = estado;
}

double Sistema::getTempoColonizado() const
{
    return this->tempoColonizado;
}

void Sistema::setTempoColonizado(double tempo)
{
    this->tempoColonizado = tempo;
}

float Sistema::dist(const Sistema* s1, const Sistema* s2)
{
    return Coord::dist(s1->getCoord(), s2->getCoord());
}

Coord Sistema::getCoord() const
{
    return this->pos;
}

void Sistema::imprimirSistema() const
{
    std::cout << "Sistema " << this->getId() << std::endl << "\t";

    Coord pos = this->getCoord();
    std::cout << "(x, y, z) = (";
    std::cout << std::setw(8) << std::fixed << pos.getX() << ", ";
    std::cout << std::setw(8) << std::fixed << pos.getY() << ", ";
    std::cout << std::setw(8) << std::fixed << pos.getZ() << ")" << std::endl;
}
